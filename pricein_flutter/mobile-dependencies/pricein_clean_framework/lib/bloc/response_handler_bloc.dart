// import 'package:pricein_clean_framework/service/response.dart';
//
// import '../service/pricein_service.dart';
// import 'bloc.dart';
//
// class ResponseHandlerBloc extends Bloc implements PriceInServiceResponseHandler {
//   static final String noListenersException =
//       'Response Handler has no listeners';
//
//   BroadcastPipe<dynamic> responseHandlerResultPipe =
//       BroadcastPipe<ResponseHandlerResult>();
//
//   @override
//   void onInvalidRequest(Error error) {
//     if (!responseHandlerResultPipe.hasListeners)
//       throw noListenersException;
//     else
//       responseHandlerResultPipe.send(ResponseHandlerResult.invalidRequest);
//   }
//
//   @override
//   void onInvalidSession() {
//     if (!responseHandlerResultPipe.hasListeners)
//       throw noListenersException;
//     else
//       responseHandlerResultPipe.send(ResponseHandlerResult.invalidSession);
//   }
//
//   @override
//   void onServerError() {
//     if (!responseHandlerResultPipe.hasListeners)
//       throw noListenersException;
//     else
//       responseHandlerResultPipe.send(ResponseHandlerResult.serverError);
//   }
//
//   @override
//   void onUnexpectedContent(Exception exception) {
//     if (!responseHandlerResultPipe.hasListeners)
//       throw noListenersException;
//     else
//       responseHandlerResultPipe.send(ResponseHandlerResult.unexpectedContent);
//   }
//
//   @override
//   void dispose() {
//     responseHandlerResultPipe.dispose();
//   }
//
//   @override
//   void onResponseError(Response response) {
//     responseHandlerResultPipe.send(response);
//   }
// }
